#!/bin/sh 
DEMO="AppDev Cloud HR Rewards Demo"
AUTHORS="Andrew Block, Eric D. Schabell"
PROJECT="git@gitlab.com:redhatdemocentral/rhcs-rewards-demo.git"
OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable/"

# Adjust these variables to point to an OCP instance.
OPENSHIFT_USER=developer
OPENSHIFT_PWD=developer
HOST_IP=api.crc.testing   # set with OCP instance hostname or IP.
HOST_APPS=apps-crc.testing
HOST_PORT=6443
OCP_PRJ=appdev-in-cloud
OCP_APP=rhcs-rewards-demo

# rhpam container configuraiton.
KIE_ADMIN_USER=erics
KIE_ADMIN_PWD=redhatpam1!
MEM_LIMIT=2Gi
VERSION=77

# rewards project details.
PRJ_ID=rewards
PRJ_REPO="https://gitlab.com/bpmworkshop/rhpam-rewards-repo.git"
DELAY=300   # waiting max 5 min various container functions to startup.

# import container functions.
source support/container-functions.sh

# wipe screen.
clear 

echo
echo "###################################################################"
echo "##                                                               ##"   
echo "##  Setting up the ${DEMO}                  ##"
echo "##                                                               ##"   
echo "##             ####  ##### ####     #   #  ###  #####            ##"
echo "##             #   # #     #   #    #   # #   #   #              ##"
echo "##             ####  ###   #   #    ##### #####   #              ##"
echo "##             #  #  #     #   #    #   # #   #   #              ##"
echo "##             #   # ##### ####     #   # #   #   #              ##"
echo "##                                                               ##"
echo "##           ####  ####   ###   #### #####  ####  ####           ##"
echo "##           #   # #   # #   # #     #     #     #               ##"
echo "##           ####  ####  #   # #     ###    ###   ###            ##"
echo "##           #     #  #  #   # #     #         #     #           ##"
echo "##           #     #   #  ###   #### ##### ####  ####            ##"
echo "##                                                               ##"
echo "##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #  ##"
echo "##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #  ##"
echo "##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #  ##"
echo "##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##  ##"
echo "##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #  ##"
echo "##                                                               ##"
echo "##           #   #  ###  #   #  ###  ##### ##### ####            ##"
echo "##           ## ## #   # ##  # #   # #     #     #   #           ##"
echo "##           # # # ##### # # # ##### #  ## ###   ####            ##"
echo "##           #   # #   # #  ## #   # #   # #     #  #            ##"
echo "##           #   # #   # #   # #   # ##### ##### #   #           ##"
echo "##                                                               ##" 
echo "##                 #### #      ###  #   # ####                   ##"
echo "##            #   #     #     #   # #   # #   #                  ##"
echo "##           ###  #     #     #   # #   # #   #                  ##"
echo "##            #   #     #     #   # #   # #   #                  ##"
echo "##                 #### #####  ###   ###  ####                   ##"
echo "##                                                               ##"   
echo "##  brought to you by,                                           ##"   
echo "##             ${AUTHORS}                    ##"
echo "##                                                               ##"   
echo "##  ${PROJECT}       ##"
echo "##                                                               ##"   
echo "###################################################################"
echo


# check for passed target IP.
if [ $# -eq 1 ]; then
	echo "Checking for host ip passed as command line variable."
	echo
	if valid_ip "$1" || [ "$1" == "$HOST_IP" ]; then
		echo "OpenShift host given is a valid IP..."
		HOST_IP=$1
		echo
		echo "Proceeding with OpenShift host: $HOST_IP..."
		echo
	else
		# bad argument passed.
		echo "Please provide a valid IP that points to an OpenShift installation..."
		echo
		print_docs
		echo
		exit
	fi
elif [ $# -gt 1 ]; then
	print_docs
	echo
	exit
elif [ $# -eq 0 ]; then
	# validate HOST_IP.
  if [ -z ${HOST_IP} ]; then
	  # no host name set yet.
	  echo "No host name set in HOST_IP..."
	  echo
		print_docs
		echo
		exit
	else
		# host ip set, echo and proceed with hostname.
		echo "You've manually set HOST to '${HOST_IP}' so we'll use that for your OpenShift Container Platform target."
		echo
	fi
fi

# make some checks first before proceeding. 
command -v oc version --client >/dev/null 2>&1 || { echo >&2 "OpenShift CLI tooling is required but not installed yet... download here (unzip and put on your path): ${OC_URL}"; exit 1; }

echo "OpenShift commandline tooling is installed..."
echo 
echo "Logging in to OpenShift as $OPENSHIFT_USER..."
echo
oc login $HOST_IP:$HOST_PORT --password=$OPENSHIFT_PWD --username=$OPENSHIFT_USER

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc login' command!"
	exit
fi

echo
echo "Check for availability of correct version of Red Hat Process Automation Manager Authoring template..."
echo
oc get templates -n openshift rhpam${VERSION}-authoring >/dev/null 2>&1

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc get template rhpam-authoring' command!"
	echo
	echo "Your container platform is mising this tempalte versoin in your catalog: rhpam${VERSION}-authoring"
	echo "Make sure you are using the correct version of Code Ready Containers as listed in project Readme file."
	echo
	exit
fi

echo "Creating a new project..."
echo
oc new-project $OCP_PRJ

echo
echo "Setting up a secrets and service accounts..."
echo
oc process -f support/app-secret-template.yaml -p SECRET_NAME=businesscentral-app-secret | oc create -f -
oc process -f support/app-secret-template.yaml -p SECRET_NAME=kieserver-app-secret | oc create -f -

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc process' command!"
	echo
	exit
fi

echo
echo "Setting up secrets link for kieserver user and password..."
echo
oc create secret generic rhpam-credentials --from-literal=KIE_ADMIN_USER=${KIE_ADMIN_USER} --from-literal=KIE_ADMIN_PWD=${KIE_ADMIN_PWD}

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc secrets' creating kieserver user and password!"
	echo
	exit
fi

echo
echo "Creating a new application using CRC catalog image..."
echo
oc new-app --template=rhpam$VERSION-authoring \
			-p APPLICATION_NAME="$OCP_APP" \
			-p CREDENTIALS_SECRET="rhpam-credentials" \
      -p BUSINESS_CENTRAL_HTTPS_SECRET="businesscentral-app-secret" \
      -p KIE_SERVER_HTTPS_SECRET="kieserver-app-secret" \
      -p BUSINESS_CENTRAL_MEMORY_LIMIT="$MEM_LIMIT"

if [ "$?" -ne "0" ]; then
	echo
	echo "Error occurred during 'oc new-app' command!"
	exit
fi

echo
echo "Testing if business central container started before adding project..."
echo

if container_ready; then
	echo
	echo "The container has started..."
	echo
else
	echo "Exiting now with Code Ready Container started, but not sure if business central"
	echo "authoring environment is ready and did not install the demo project."
	echo
	exit
fi

echo "Creating a space for the project import..."
echo

if create_project_space; then
  echo "Creation of new space for project import started..."
  echo
else
  echo "Exiting now with Code Ready Container started, but not sure if business central"
  echo "authoring environment is ready and did not install the demo project."
	echo 
	exit
fi

echo "Validating new project space creation..."
echo

if validate_project_space; then
	echo "Creation of space successfully validated..."
	echo
else
	echo "Exiting now with Code Ready Container started, business central"
	echo "authoring environment is ready, but unable to import the demo project."
	echo
	exit
fi

echo "Checking if project already exists, otherwise add it..."
echo

if project_exists; then
	echo "Demo project already exists..."
	echo 
else
	echo "Project does not exist, importing in to container..."
	echo
	
	if project_imported; then
		echo "Imported project successfully..."
		echo
	else
	  echo "Exiting now with Code Ready Container started, business central"
	  echo "authoring environment is ready, but unable to import the demo project."
	  echo
		exit
	fi
fi

echo "================================================================================="
echo "=                                                                               ="
echo "=  Login to Red Hat Process Automation Manager to start exploring the HR        ="
echo "=  rewards project at:                                                          ="
echo "=                                                                               ="
echo "=   https://${OCP_APP}-rhpamcentr-${OCP_PRJ}.${HOST_APPS}       ="
echo "=                                                                               ="
echo "=    Log in: [ u:erics / p:redhatpam1! ]                                        ="
echo "=                                                                               ="
echo "=    Others:                                                                    ="
echo "=            [ u:kieserver / p:redhatpam1! ]                                    ="
echo "=            [ u:caseuser / p:redhatpam1! ]                                     ="
echo "=            [ u:casemanager / p:redhatpam1! ]                                  ="
echo "=            [ u:casesupplier / p:redhatpam1! ]                                 ="
echo "=                                                                               ="
echo "================================================================================="
echo

