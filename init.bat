@ECHO OFF
setlocal

set PROJECT_HOME=%~dp0
set DEMO=AppDev Cloud HR Rewards Demo
set AUTHORS=Andrew Block, Eric D. Schabell
set PROJECT=git@gitlab.com:redhatdemocentral/rhcs-rewards-demo.git
set OC_URL="https://mirror.openshift.com/pub/openshift-v4/clients/ocp/stable/openshift-client-windows-4.5.8.zip"

REM Adjust these variables to point to an OCP instance.
set OPENSHIFT_USER=developer
set OPENSHIFT_PWD=developer
set HOST_IP=api.crc.testing   # set with OCP instance hostname or IP.
set HOST_APPS=apps-crc.testing
set HOST_PORT=6443
set OCP_PRJ=appdev-in-cloud
set OCP_APP=rhcs-rewards-demo

REM Rhpam container configuraiton.
set KIE_ADMIN_USER=erics
set KIE_ADMIN_PWD=redhatpam1!
set MEM_LIMIT=2Gi
set VERSION=77

REM Rewards project details.
set PRJ_ID=rewards
set PRJ_REPO=https://gitlab.com/bpmworkshop/rhpam-rewards-repo.git

REM Waiting max 5 min various container functions to startup.
set DELAY=300  

REM import container functions.
call support\container-functions.bat

REM wipe screen.
cls

echo.
echo ###################################################################
echo ##                                                               ##
echo ##  Setting up the %DEMO%                  ##
echo ##                                                               ##   
echo ##             ####  ##### ####     #   #  ###  #####            ##
echo ##             #   # #     #   #    #   # #   #   #              ##
echo ##             ####  ###   #   #    ##### #####   #              ##
echo ##             #  #  #     #   #    #   # #   #   #              ##
echo ##             #   # ##### ####     #   # #   #   #              ##
echo ##                                                               ##
echo ##           ####  ####   ###   #### #####  ####  ####           ##
echo ##           #   # #   # #   # #     #     #     #               ##
echo ##           ####  ####  #   # #     ###    ###   ###            ##
echo ##           #     #  #  #   # #     #         #     #           ##
echo ##           #     #   #  ###   #### ##### ####  ####            ##
echo ##                                                               ##
echo ##   ###  #   # #####  ###  #   #  ###  ##### #####  ###  #   #  ##
echo ##  #   # #   #   #   #   # ## ## #   #   #     #   #   # ##  #  ##
echo ##  ##### #   #   #   #   # # # # #####   #     #   #   # # # #  ##
echo ##  #   # #   #   #   #   # #   # #   #   #     #   #   # #  ##  ##
echo ##  #   # #####   #    ###  #   # #   #   #   #####  ###  #   #  ##
echo ##                                                               ##
echo ##           #   #  ###  #   #  ###  ##### ##### ####            ##
echo ##           ## ## #   # ##  # #   # #     #     #   #           ##
echo ##           # # # ##### # # # ##### #  ## ###   ####            ##
echo ##           #   # #   # #  ## #   # #   # #     #  #            ##
echo ##           #   # #   # #   # #   # ##### ##### #   #           ##
echo ##                                                               ## 
echo ##                 #### #      ###  #   # ####                   ##
echo ##            #   #     #     #   # #   # #   #                  ##
echo ##           ###  #     #     #   # #   # #   #                  ##
echo ##            #   #     #     #   # #   # #   #                  ##
echo ##                 #### #####  ###   ###  ####                   ##
echo ##                                                               ##   
echo ##  brought to you by,                                           ##   
echo ##             %AUTHORS%                    ##
echo ##                                                               ##
echo ##  %PROJECT%       ##
echo ##                                                               ##
echo ###################################################################
echo.

REM Validate OpenShift
set argTotal=0

for %%i in (%*) do set /A argTotal+=1

if %argTotal% EQU 1 (

    call :validateIP %1 valid_ip

	if !valid_ip! EQU 0 (
	    echo OpenShift host given is a valid IP...
	    set HOST_IP=%1
		echo.
		echo Proceeding with OpenShift host: !HOST_IP!...
	) else (
		echo Please provide a valid IP that points to an OpenShift installation...
		echo.
        GOTO :printDocs
	)

)

if %argTotal% GTR 1 (
    GOTO :printDocs
)

if %argTotal% EQU 0 (
	if [%HOST_IP%] == [] (
		GOTO :printDocs
	)

	echo.
	echo Assuming you set a valid host, so proceeding with: %HOST_IP%
	echo.
)


REM make some checks first before proceeding.	
call oc version --client >nul 2>&1
if  %ERRORLEVEL% NEQ 0 (
	echo OpenShift command line tooling is required but not installed yet... download here: %OC_URL%
	echo.
	GOTO :EOF
)

echo OpenShift commandline tooling is installed...
echo.
echo Logging in to OpenShift as %OPENSHIFT_USER%...
echo.
call oc login %HOST_IP%:%HOST_PORT% --password="%OPENSHIFT_PWD%" --username="%OPENSHIFT_USER%"

if not "%ERRORLEVEL%" == "0" (
  echo.
	echo Error occurred during 'oc login' command!
	echo.
	GOTO :EOF
)

echo.
echo Check for availability of correct version of Red Hat Process Automation Manager Authoring template...
echo.
call oc get templates -n openshift rhpam%VERSION%-authoring 

if not %ERRORLEVEL% == 0 (
	echo.
	echo Error occurred during 'oc get template rhpam-authoring' command!
	echo.
	echo Your container platform is mising this tempalte versoin in your catalog: rhpam%VERSION%-authoring
	echo Make sure you are using the correct version of Code Ready Containers as listed in project Readme file
	echo.
	GOTO :EOF
)

echo.
echo Creating a new project...
echo.
call oc new-project %OCP_PRJ%

echo.
echo Setting up a secrets...
echo.
call oc process -f support/app-secret-template.yaml | oc create -f -

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc process' app-secret command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up kieserver secret...
echo.
call oc process -f support/app-secret-template.yaml -p SECRET_NAME=kieserver-app-secret | oc create -f -

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc process' kieserver app-secret command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up business central service account...
echo.
call oc create serviceaccount businesscentral-service-account

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc create' bc service account command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up kieserver service account...
echo.
call oc create serviceaccount kieserver-service-account

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc create' kieserver command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up secrets link to business central service account...
echo.
call oc secrets link --for=mount businesscentral-service-account businesscentral-app-secret

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc secrets' link bc service account command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up secrets link to kieserver service account...
echo.
call oc secrets link --for=mount kieserver-service-account kieserver-app-secret

if not "%ERRORLEVEL%" == "0" (
	echo.
	echo Error occurred during 'oc secrets' link kieserver service command!
	echo.
	GOTO :EOF
)

echo.
echo Setting up secrets link for kieserver user and password...
echo.
call oc create secret generic rhpam-credentials --from-literal=KIE_ADMIN_USER=%KIE_ADMIN_USER% --from-literal=KIE_ADMIN_PWD=%KIE_ADMIN_PWD%

if not "%ERRORLEVEL%" == "0" (
  echo.
  echo Error occurred during 'oc secrets' creating kieserver user and password!
  echo.
  GOTO :EOF
)

echo.
echo Creating a new application using CRC catalog image...
echo.
call oc new-app --template=rhpam%VERSION%-authoring -p APPLICATION_NAME="%OCP_APP%" -p CREDENTIALS_SECRET="rhpam-credentials" -p BUSINESS_CENTRAL_HTTPS_SECRET="businesscentral-app-secret" -p KIE_SERVER_HTTPS_SECRET="kieserver-app-secret" -p BUSINESS_CENTRAL_MEMORY_LIMIT="%MEM_LIMIT%"

if not "%ERRORLEVEL%" == "0" (
  echo.
	echo Error occurred during 'oc new-app' command!
	echo.
	GOTO :EOF
)

echo.
echo =================================================================================
echo =                                                                               =
echo =  Login to Red Hat Process Automation Manager to start exploring the HR        =
echo =  rewards project at:                                                          =
echo =                                                                               =
echo =  https://%OCP_APP%-rhpamcentr-%OCP_PRJ%.%HOST_APPS%   =
echo =                                                                               =
echo =    Log in: [ u:erics / p:redhatpam1! ]                                        =
echo =                                                                               =
echo =    Others:                                                                    =
echo =            [ u:kieserver / p:redhatpam1! ]                                    =
echo =            [ u:caseuser / p:redhatpam1! ]                                     =
echo =            [ u:casemanager / p:redhatpam1! ]                                  =
echo =            [ u:casesupplier / p:redhatpam1! ]                                 =
echo =                                                                               =
echo =  Note: it takes a few minutes to start the containers...                      =
echo =                                                                               =
echo =================================================================================
echo.

endlocal

